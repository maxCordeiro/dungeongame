﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MaxDungeon;

public class CameraBob : MonoBehaviour
{
    PlayerController pController;

    float timer;

    public float bobSpeed, bobMultiplier;

    // Start is called before the first frame update
    void Start()
    {
        pController = transform.parent.parent.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        float wave = 0.0f;

        if (pController.velocity != Vector3.zero)
        {
            wave = Mathf.Sin(timer);
            timer += bobSpeed;
            if (timer > Mathf.PI * 2)
            {
                timer -= (Mathf.PI * 2);
            }
        } else
        {
            timer = 0.0f;
        }

        Vector3 v3 = transform.localPosition;

        if (wave != 0.0f)
        {
            float transformChange = wave * bobMultiplier;

            v3.y += transformChange;
        } else
        {
            v3.y = 0.0f;
        }

        transform.localPosition = v3;
    }
}
