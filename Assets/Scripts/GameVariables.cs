﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MaxDungeon
{
    public static class GameVariables
    {
        public static Dictionary<string, Item> items = new Dictionary<string, Item>()
        {
            {"Sword", new Weapon(10) }
        };
    }
}