﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MaxDungeon;

public class Enemy : MonoBehaviour
{
    CharacterController charCont;
    Animator anim;
    public int hp, attack;
    bool queueDeath;

    public void Start()
    {
        anim = GetComponent<Animator>();
        charCont = GetComponent<CharacterController>();
    }

    public void Update()
    {
        if (queueDeath)
        {
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("enemy_hurt"))
            {
                OnDeath();
            }
        }
    }

    public void OnHurt(Item _weapon)
    {
        if (queueDeath || _weapon is Weapon) return;

        anim.SetTrigger("hurt");
        hp -= _weapon.attackDamage;

        if (hp <= 0)
        {
            queueDeath = true;
        }
    }
    
    public void OnDeath()
    {
        queueDeath = false;
        Destroy(gameObject);
    }
}
