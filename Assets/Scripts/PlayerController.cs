﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MaxDungeon
{
    public class PlayerController : MonoBehaviour
    {
        CharacterController charCont;
        Vector2 input, mouseMovement;
        public Vector3 velocity;

        public int moveSpeed;
        public int mouseSensitivity;

        float horizontalAngle, verticleAngle;

        public Transform charRoot, weaponHolder;

        float tempRecharge;
        bool tempAttacked;
        public float defaultRange;

        Animator anim;
        
        public Image[] hearts;

        // Start is called before the first frame update
        void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            charCont = GetComponent<CharacterController>();
            anim = GetComponent<Animator>();

            PlayerVariables.EquipItem((Weapon)GameVariables.items["Sword"]);
        }

        // Update is called once per frame
        void Update()
        {
            Move();
            Look();

            if (Input.GetButtonDown("Fire"))
            {
                Attack();
            }

            Recharge();

        }

        public void Move()
        {
            input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            velocity.x = input.x;

            if (velocity.sqrMagnitude > 1.0f)
            {
                velocity.Normalize();
            }

            velocity.z = input.y;
            velocity.y -= 10 * Time.deltaTime;
            velocity *= moveSpeed;
            velocity *= Time.deltaTime;
            velocity = transform.TransformDirection(velocity);

            charCont.Move(velocity);
        }

        public void Look()
        {
            mouseMovement = new Vector2(Input.GetAxis("Mouse X"), -Input.GetAxis("Mouse Y")) * mouseSensitivity;
            horizontalAngle += mouseMovement.x;

            if (horizontalAngle > 360) horizontalAngle -= 360.0f;
            if (horizontalAngle < 0) horizontalAngle += 360.0f;

            Vector3 currentAngles = transform.localEulerAngles;
            currentAngles.y = horizontalAngle;
            transform.localEulerAngles = currentAngles;

            verticleAngle = Mathf.Clamp(verticleAngle + mouseMovement.y, -89.0f, 89.0f);
            currentAngles = charRoot.transform.localEulerAngles;
            currentAngles.x = verticleAngle;
            charRoot.transform.localEulerAngles = currentAngles;
        }

        public void Attack()
        {
            if (!tempAttacked)
            {
                anim.SetTrigger("attack");

                Ray r = new Ray(charRoot.transform.position, charRoot.transform.forward);
                RaycastHit hit;

                if (Physics.Raycast(r, out hit, defaultRange))
                {
                    Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                    Debug.Log("Hit");

                    Enemy hitEnemy = hit.transform.gameObject.GetComponent<Enemy>();
                    if (hitEnemy)
                    {
                        hitEnemy.OnHurt(PlayerVariables.heldItem);
                    }
                }

                tempAttacked = true;
            }
        }

        public void Recharge()
        {
            if (tempAttacked)
            {
                tempRecharge += Time.deltaTime;

                if (tempRecharge >= 0.6f)
                {
                    tempRecharge = 0;
                    tempAttacked = false;
                }
            }
        }

        public void OnDrawGizmos()
        {
            Matrix4x4 m = Matrix4x4.TRS(charRoot.transform.forward * defaultRange, charRoot.transform.rotation, Vector3.one);
            Gizmos.color = Color.magenta;
            //Gizmos.matrix = m;
            Gizmos.DrawLine(charRoot.transform.position, charRoot.transform.position + charRoot.transform.forward * defaultRange);
        }

        public void UpdateHealthDisplay()
        {
            for (int i = 0; i < hearts.Length; i++)
            {
                if ((i + 1) < PlayerVariables.health)
                {

                }
            }
        }
    }

}
