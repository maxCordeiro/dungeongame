﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MaxDungeon
{
    public static class PlayerVariables
    {
        public static int health = 10;
        public static Item heldItem = null;

        public static void EquipItem(Item _item)
        {
            heldItem = _item;
        }
    }
}
