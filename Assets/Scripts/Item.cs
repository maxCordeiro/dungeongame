﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MaxDungeon
{
    public class Item
    {
        public string name;
        public int cost, weight;
    }

    public class Weapon : Item
    {
        public int attackDamage;
        public float reachOverride = -1;

        public Weapon(int _attackDamage, float _reachOverride = -1)
        {
            attackDamage = _attackDamage;
            reachOverride = _reachOverride;
        }
    }
}